#include<iostream>
#include<string>

class User
{
private:
	std::string firstname;
	std::string lastname;
	std::string phonenumber;
	std::string email;
public:
	User();
	User(std::string fn, std::string ln, std::string phn, std::string em);
	User(const User& user);
	~User();
	void SetFirstName(std::string fn);
	void SetLastName(std::string ln);
	void SetPhoneNumber(std::string phn);
	void SetEmail(std::string em);
	std::string GetFirstName();
	std::string GetLastName();
	std::string GetPhoneNumber();
	std::string GetEmail();
	std::string Output();
};
