#include "User.h"
#include<fstream>
#include <vector>

class Organizer
{
private:
	std::vector<User> VectorOfUsers;
public:
	Organizer();
	Organizer(const Organizer& organizer);
	Organizer(std::vector<User> newvector);
	void ReadFromFile(std::string filename);
	void WriteToFile(std::string filename);
	void Output();
	void Output(std::string fn);
	void Delete(std::string fn);
	~Organizer();

};
