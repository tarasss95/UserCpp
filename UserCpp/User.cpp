#include "User.h"

User::User()
{
	firstname = std::string();
	lastname = std::string();
	phonenumber = std::string();
	email = std::string();
}
User::User(std::string fn, std::string ln, std::string phn, std::string em)
{
	firstname = fn;
	lastname = ln;
	phonenumber = phn;
	email = em;
}
User::User(const User& user)
{
	firstname = user.firstname;
	lastname = user.lastname;
	phonenumber = user.phonenumber;
	email = user.email;
}
void User::SetFirstName(std::string fn)
{
	firstname = fn;
}
void User::SetLastName(std::string ln)
{
	lastname = ln;
}
void User::SetPhoneNumber(std::string phn)
{
	phonenumber = phn;
}
void User::SetEmail(std::string em)
{
	email = em;
}
std::string User::GetFirstName()
{
	return firstname;
}
std::string User::GetLastName()
{
	return lastname;
}
std::string User::GetPhoneNumber()
{
	return phonenumber;
}
std::string User::GetEmail()
{
	return email;
}
std::string User::Output()
{
	return firstname + " " + lastname + " " + phonenumber + " " + email;
}
User::~User()
{
}