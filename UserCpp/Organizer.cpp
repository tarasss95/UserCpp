#include"Organizer.h"

Organizer::Organizer()
{}
Organizer::Organizer(const Organizer& organizer)
{
	VectorOfUsers = organizer.VectorOfUsers;
}
Organizer::Organizer(std::vector<User> newvector)
{
	VectorOfUsers = newvector;
}
void Organizer::ReadFromFile(std::string filename)
{
	std::ifstream fin(filename);
	if (!fin.is_open())
	{
		std::cout << "file can't be opened.\n";
	}
	else
	{
		VectorOfUsers.clear();
		std::string fn,ln,phn,em;
		while (!fin.eof())
		{
			if (fin >> fn >> ln >> phn >> em)
			{
				User user = User(fn, ln, phn, em);
				VectorOfUsers.push_back(user);
			}
		}
		fin.close();
	}
	
}
void Organizer::WriteToFile(std::string filename)
{
	std::ofstream fout(filename);
	if (!fout.is_open())
	{
		std::cout << "file can't be opened.\n";
	}
	else
	{
		for each  (User user in VectorOfUsers)
		{
			fout << user.Output()<<std::endl;
		}
		fout.close();
	}
}
void Organizer::Output()
{
	for each  (User user in VectorOfUsers)
	{
		std::cout << user.Output() << std::endl;
	}
}
void Organizer::Output(std::string fn)
{
	for each  (User user in VectorOfUsers)
	{
		if (user.GetFirstName() == fn)
		{
			std::cout << user.Output() << std::endl;
		}
	}
}
void Organizer::Delete(std::string fn)
{
	for (auto it = VectorOfUsers.begin(); it != VectorOfUsers.end(); )
	{
		if (it->GetFirstName() == fn)
			it = VectorOfUsers.erase(it);
		else
			++it;
	}
}
Organizer::~Organizer()
{

}